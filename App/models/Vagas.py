from App import db

class Vaga(db.Model):
    __tablename__ = 'VAGA'
    __table_args__ = {'schema': 'flaskbase_db', 'autoload': True, 'autoload_with': db.engine}

class Ticket(db.Model):
    __tablename__ = 'TICKET'
    __table_args__ = {'schema': 'flaskbase_db', 'autoload': True, 'autoload_with': db.engine}

# declaring VIEW :
class VW_Vaga(db.Model):
    __tablename__ = 'VW_VAGA'
    __table_args__ = {'schema': 'flaskbase_db', 'autoload': True, 'autoload_with': db.engine}
    IDVAGA = db.Column(db.Integer, nullable=False, primary_key=True)

