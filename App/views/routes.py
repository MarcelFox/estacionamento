from flask import g, Blueprint, render_template, request, redirect, url_for, jsonify
from flask import current_app as app
from App import db
from App.models.Vagas import Vaga, VW_Vaga
import requests

session = db.session()

index = Blueprint('index', __name__)

@index.route('/', methods=['POST', 'GET'])
def main_page():
    return render_template('index.html')
    
@index.route('/ticket', methods=['POST', 'GET'])
def ticket():
    if request.method=='POST':
        TIPO = request.form['poll_option']
        
        g.TIPO = TIPO
        
        menor = session.query(
        VW_Vaga, db.func.min(VW_Vaga.DISTANCIA
            ).label('distancia')).filter(VW_Vaga.TIPO == TIPO).one()

        vagas_livres = session.query(
            VW_Vaga).filter(VW_Vaga.DISTANCIA == menor.distancia).all()

        return render_template('ticket.html', vaga=vagas_livres)

    return render_template('ticket.html')

@index.route('/entrada', methods=['POST', 'GET'])
def entrada():
    TIPO = request.form['entrada']
    app.logger.info(TIPO)

    connection = db.engine.raw_connection()
    try:
        cursor = connection.cursor()
        cursor.callproc("SP_EMITIR_TICKET", ['NORMAL'])
        results = list(cursor.fetchall())
        cursor.close()
        connection.commit()
    finally:
        connection.close()

    

    vaga = session.query(Vaga).all()

    return render_template('entrada.html', tipo=TIPO, vaga=vaga)

@index.route('/pagamento', methods=['POST', 'GET'])
def pagamento():
    TIPO = request.form['pagamento']
    app.logger.info(TIPO)

    connection = db.engine.raw_connection()
    try:
        cursor = connection.cursor()
        cursor.callproc("SP_PAGAR_TICKET", ['1'])
        results = list(cursor.fetchall())
        cursor.close()
        connection.commit()
    finally:
        connection.close()

    vaga = session.query(Vaga).all()

    return render_template('pagamento.html', tipo=TIPO, vaga=vaga)

@index.route('/saida', methods=['POST', 'GET'])
def saida():
    TIPO = request.form['saida']
    app.logger.info(TIPO)

    connection = db.engine.raw_connection()
    try:
        cursor = connection.cursor()
        cursor.callproc("SP_SAIDA", ['1'])
        results = list(cursor.fetchall())
        cursor.close()
        connection.commit()
    finally:
        connection.close()

    vaga = session.query(Vaga).all()

    return render_template('saida.html', tipo=TIPO, vaga=vaga)


# @index.route('/ticket', methods=['POST'])
# def ticket():
#     g.TIPO = request.form['poll_option']
#     #app.logger.info(g.TIPO)
    
#     r = requests.post('http://localhost:8080/', g.TIPO)

#     return jsonify({
#         'TIPO': g.TIPO})

app.register_blueprint(index)